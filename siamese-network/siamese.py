import torch
import torch.nn as nn
import torch.functional as F
from torch.autograd import Variable
import torch.optim as optim
import os
import cPickle as pickle
import gensim
import numpy as np

os.environ["CUDA_VISIBLE_DEVICES"] = "2"

print "testing"
model_loaded = gensim.models.doc2vec.Doc2Vec.load('doc2vec_model.doc2vec')
print "loading success"

class MySiamese3(nn.Module):
    def __init__(self,hdim):
        super(MySiamese3,self).__init__()
        self.lstm = nn.LSTM(300,hdim)
        
    def forward(self,batch,lengths):
        #print type(batch)
        #print batch
        bsize = batch.size()[1]
        pack = nn.utils.rnn.pack_padded_sequence(batch, lengths)

        h = (Variable(torch.zeros(1,bsize,hdim).cuda()),Variable(torch.zeros(1,bsize,hdim).cuda()))
        out,h = self.lstm(pack,h)
        #unpacked, unpacked_len = nn.utils.rnn.pad_packed_sequence(h)
        
        result = h[0][0]
        #print h[0][0].size()
        
        return result
    
    
vec_for_comma = np.random.rand(300).astype(np.float32)

def get_embedding(batch):
    embed = []
    for i in range(0,len(batch)):
        sample = batch[i]
        dmtr = np.zeros((len(sample), 300), dtype = np.float32)
        for j in range(0,len(sample)):
            if sample[j]=='':
                continue;
            if sample[j]==',':
                dmtr[j] = np.random.rand(300).astype(np.float32)
                continue;
            if sample[j]=='\n':
                continue;
            dmtr[j] = model_loaded.infer_vector(sample[j])
        embed.append(dmtr)

    return embed;

def addpadding(batch):
    n_samples = len(batch)
    batch1 = []
    batch2 = []
    similarity = []
    for i in range(0,len(batch)):
        batch1.append(batch[i][0])
        batch2.append(batch[i][1])
        similarity.append(batch[i][2])

    

    max_len = 0
    batch1_with_padding = []
    batch2_with_padding = []
    for i in range(len(batch)):
        max_len = max(len(batch1[i].split('.')), len(batch2[i].split('.')))
        sent1 = batch1[i].split('.')
        sent2 = batch2[i].split('.')
        while(len(sent1)<max_len):
            sent1.append(',')
        while(len(sent2)<max_len):
            sent2.append(',')
        batch1_with_padding.append(sent1)
        batch2_with_padding.append(sent2)
        
    batch1_with_padding.sort(key=len, reverse=True)
    batch2_with_padding.sort(key=len, reverse=True)
    
    lengths = []
    for line in batch1_with_padding:
        lengths.append(len(line))
    max_len = max(lengths)
    
    new_batch1 = []
    new_batch2 = []
    for sample in batch1_with_padding:
        sentences = sample
        while(len(sample)<max_len):
            sentences.append(',')
        new_batch1.append(sentences)

    
    for sample in batch2_with_padding:
        sentences = sample
        while(len(sentences)<max_len):
            sentences.append(',')
        new_batch2.append(sentences)
    new_batch1 = np.array(new_batch1)
    new_batch2 = np.array(new_batch2)
    
    similarity = np.array(similarity)
    return new_batch1,new_batch2,lengths,similarity

    
    
    
traindata = pickle.load(open("70000_sentence_train.p","rb"))
testdata = pickle.load(open("9703_sentence_test.p","rb"))


max_number_of_epochs = 10
hdim = 50
batchsize =  32
n_samples = len(traindata)
model = MySiamese3(hdim)
model.cuda()
ff = open("parameters.txt",'w+')
loss_fn = nn.MSELoss()
optimizer = optim.Adadelta(model.parameters(),lr=0.00)
for epoch in range(0,max_number_of_epochs):
    for i in range(0,n_samples,batchsize):
        max_sample_number = min(n_samples,i+batchsize)
        batch = traindata[i:max_sample_number]
        batch1, batch2, lengths, target = addpadding(batch)
        #batch1 = np.array([traindata[i][0]])
        #batch2 = np.array([traindata[i][1]])
        #target = np.array([traindata[i][2]])
        embed1 = get_embedding(batch1)
        embed2 = get_embedding(batch2)
        #b1,b2,target = addp(batch)
        embed1 = np.dstack(embed1)
        embed2 = np.dstack(embed2)
        
        embed1 = np.swapaxes(embed1,1,2)
        embed2 = np.swapaxes(embed2,1,2)
        
        optimizer.zero_grad()
        out1 = model(Variable(torch.from_numpy(embed1).cuda()), lengths)
        out2 = model(Variable(torch.from_numpy(embed2).cuda()), lengths)
        diff = (out1-out2).norm(p=1,dim=1)
        diff = torch.exp(-diff)
        diff1 = torch.clamp(diff, min=1e-7, max=1.0-1e-7)
        
        target1 = Variable(torch.from_numpy(target).type(torch.FloatTensor).cuda())
        target2 = torch.clamp((target1-1.0)/4.0, min=1e-7, max=1.0-1e-7)
        loss = torch.mean((diff1-target2)**2)
        #pp = [p for p in model.parameters()]
        
        #print "parameters before backpropogation:",pp
        loss.backward()
        optimizer.step()
        
        #print "parameters after back",pp
        print epoch,i,"loss ; ",loss.data[0]
        if i>1000:
            break
        pp = [p for p in  model.parameters()]
        print pp
    break
    test_samples = len(testdata)
    error = 0
    num_hidden = batchsize
    for j in range(0,test_samples,batchsize):
        max_sample_number = min(j+batchsize, test_samples)
        batch = testdata[j:max_sample_number]
        if batchsize!=max_sample_number-j:
            num_hidden = max_sample_number-j
        test_batch1, test_batch2, lengths,test_target = addpadding(batch)
        embed1 = get_embedding(test_batch1)
        embed2 = get_embedding(test_batch2)
        embed1 = np.dstack(embed1)
        embed2 = np.dstack(embed2)
        embed1 = np.swapaxes(embed1,1,2)
        embed2 = np.swapaxes(embed2,1,2)
            #print "Embedding 1 is:",embed1
            #print "Embedding2 is",embed2
        optimizer.zero_grad()
        #hidden = (Variable(torch.zeros(1, num_hidden, hdim), requires_grad=False),Variable(torch.zeros(1, num_hidden, hdim), requires_grad=False))
        out1 = model(Variable(torch.from_numpy(embed1).cuda()), lengths)
        #hidden = (Variable(torch.zeros(1, num_hidden, hdim), requires_grad=False),Variable(torch.zeros(1, num_hidden, hdim), requires_grad=False))
        out2 = model(Variable(torch.from_numpy(embed2).cuda()), lengths)
            
        diff = (out1-out2).norm(p=1, dim=1)
        diff = torch.exp(-diff)
        diff1 = diff.data
        diff2 = diff1.cpu().numpy()
        diff2 = np.clip(diff2, 1e-7,1.0-1e-7)
        test_target = np.clip((test_target-1.0)/4.0, 1e-7,1.0-1e-7)
        actualdiff = abs(diff2-test_target)
        actualdiff = actualdiff
            
        error = error + np.sum(actualdiff)
    print "error after ",epoch,"is",error
    pp = [p for p in  model.parameters()]
    break
    
ff.close()